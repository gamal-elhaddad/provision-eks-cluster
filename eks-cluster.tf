provider "kubernetes" {
    host = data.aws_eks_cluster.my-eks-cluster.endpoint
    token = data.aws_eks_cluster_auth.my-eks-cluster.token
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.my-eks-cluster.certificate_authority[0].data)
}

data "aws_eks_cluster" "my-eks-cluster" {
    name = module.eks-cluster.cluster_id
}

data "aws_eks_cluster_auth" "my-eks-cluster" {
    name = module.eks-cluster.cluster_id
}
module "eks-cluster" {
  source  = "terraform-aws-modules/eks/aws"
  version = "18.23.0"

  cluster_name = "my-eks-cluster"
  cluster_version = "1.21"

  subnet_ids = module.myapp-vpc.private_subnets
  vpc_id = module.myapp-vpc.vpc_id

  tags = {
    environment = "development"
    application = "myapp"
  }

  eks_managed_node_groups = {
    dev = {
        min_size = 1
        max_size = 3
        desired_size = 2
        instance_types = ["t2.small"]
    }
  }
}
