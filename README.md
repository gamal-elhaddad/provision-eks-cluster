1- Create IAM Role for the EKS to allow access to other AWS service resources that are required to operate Clusters managed by the EKS Cluster.  



2- Create the VPC which will have the Worker Nodes.



3- Create the EKS which is the Master Node.



4- Create IAM Role for the NodeGroup of the Worker Nodes with appropriate policies.



5- Create NodeGroup of EC2 Instances which are the Worker Nodes inside the VPC and attach them to the EKS Cluster(the Master Node).



6- Configure Auto Scaling for the cluster.



7- Deploy an Nginx container to the cluster through creating Deployment.


After clonning the Repo you can do this :

_ terraform init

_ terraform plan

_ terraform apply --auto-approve

_ kubectl apply -f nginx-config.yaml

then clean up with : terraform destroy
